package main

import (
	"fmt"
	"log"
	"net/http"
)

//example of try catch alternative to error handling.
func recoverHandler(w http.ResponseWriter, r *http.Request) {
	if rec := recover(); rec != nil {
		fmt.Println("Recovered from failure", rec)
		log.Println(rec)
		http.Redirect(w, r, "/path", 302)
		l, _ := template.Get("default")
		l.Render(w, getModelData())
	}
}

//Custom404 Error Page
func Custom404(w http.ResponseWriter, r *http.Request) {
	defer recoverHandler(w, r)

	fmt.Println("CUSTOM 404 ERROR PAGE.....SHOULD NOT SHOW ON KNOWN ROUTES")
	fmt.Println(r.URL)
	fmt.Println("route not found")
	fmt.Fprintf(w, "404 NOT FOUND \n")
}

//Hello World Response
func Hello(w http.ResponseWriter, r *http.Request) {
	defer recoverHandler(w, r)

	fmt.Println(r.URL)
	fmt.Println("Calling handler.")
	l, _ := template.Get("default33")
	l.Render(w, getModelData())
	fmt.Println("Returned normally from handler.")
}
