package main

import (
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/krobolt/go-themes"
	"gitlab.com/krobolt/go-webapp"
)

var template themes.Theme

func createTemplate() {
	template := themes.NewTheme()
	baseLayout, _ := themes.NewLayout("base", []string{
		"view/base.html",
		"views/body.html",
	})
	template.Add(baseLayout)
}

func serveAssets(h *mux.Router) {
	s := http.StripPrefix("/static/", http.FileServer(http.Dir("./static/")))
	h.PathPrefix("/static/").Handler(s)
}

func serveImages(h *mux.Router) {
	imageFileServer := http.StripPrefix("/images/", http.FileServer(http.Dir("./static/images/")))
	h.PathPrefix("/images/").Handler(
		cacheControlWrapper(60*60*7*30, imageFileServer),
	)
}

func serve404(h *mux.Router) {
	h.NotFoundHandler = http.HandlerFunc(Custom404)
}

func main() {

	app := webapp.NewApp()

	createTemplate()

	//working with the handler
	handler := app.GetHandler()
	serve404(handler)
	serveAssets(handler)
	serveImages(handler)

	app.Set("log", LoggingMiddleware)

	app.Add("GET", "/", Hello, []string{"log"})

	app.Group("/dir/", func() {
		app.Add("GET", "page1", Hello, []string{})
		app.Add("GET", "page2", Hello, []string{})
	}, []string{"log"})

	app.Serve(":80")
}

func cacheControlWrapper(expires int, h http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			es := strconv.Itoa(expires)
			w.Header().Set("Cache-Control", "max-age="+es)
			h.ServeHTTP(w, r)
		},
	)
}

//LoggingMiddleware Log Middleware
func LoggingMiddleware() webapp.Middleware {
	// Create a new Middleware
	return func(f http.HandlerFunc) http.HandlerFunc {
		// Define the http.HandlerFunc
		return func(w http.ResponseWriter, r *http.Request) {
			// Do middleware things
			start := time.Now()
			defer func() { log.Println(r.URL.Path, time.Since(start)) }()
			// Call the next middleware/handler in chain
			f(w, r)
		}
	}
}
