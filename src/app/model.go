package main

type todo struct {
	Title string
	Done  bool
}
type todoPageData struct {
	PageTitle string
	Todos     []todo
}

func getModelData() todoPageData {
	return todoPageData{
		PageTitle: "My TODO list",
		Todos: []todo{
			{Title: "Task 1", Done: false},
			{Title: "Task 2", Done: true},
			{Title: "Task 3", Done: true},
		},
	}
}
