package main

import (
	"fmt"
	"time"

	"github.com/go-redis/redis"
)

type redisCache struct {
	c *redis.Client
}

func (r *redisCache) Test() bool {
	p, _ := r.c.Ping().Result()
	if p == "PONG" {
		return true
	}
	return false
}

func (r *redisCache) Set(key string, data interface{}, expires time.Duration) (bool, error) {
	err := r.c.Set(key, data, expires).Err()
	if err != nil {
		fmt.Println("Unable to store value")
		return false, err
	}
	return true, err
}

func (r *redisCache) Get(key string) (interface{}, bool) {
	v, err := r.c.Get(key).Result()

	if err == redis.Nil {
		fmt.Println("key2 does not exist")
		return nil, false
	} else if err != nil {
		return nil, false
	} else {
		return v, true
	}
}

func (r *redisCache) GetClient() *redis.Client {
	return r.c
}

func edis() {
	client := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	r := &redisCache{
		c: client,
	}

	fmt.Println(r.Test())

	ok, _ := r.Set("som-key", "somevalue", 0)
	fmt.Println(ok)

	va, ok := r.Get("som-key")
	if ok {
		fmt.Println("ok:", va)
	}
}
