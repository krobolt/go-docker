package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

type db struct {
	conn string
}

//DB Database Interface
type DB interface {
	Test() bool
}

//NewDB create new database.
func NewDB() DB {
	return &db{
		conn: getEnvConn(),
	}
}

func (d *db) Test() bool {
	db, _ := sql.Open("mysql", d.conn)

	defer func() {
		db.Close()
	}()

	if db.Ping() != nil {
		return false
	}
	return true
}

func getEnvConn() string {
	//c := "root:Securepass@tcp(maria_db:3306)/mysql"
	return os.Getenv("DB_USER") + ":" + os.Getenv("DB_PASSWORD") + "@tcp(" + os.Getenv("DB_HOST") + ")/" + os.Getenv("DB_TABLE")
}

func helloWorld(w http.ResponseWriter, r *http.Request) {

	d := &db{
		conn: getEnvConn(),
	}
	d.Test()

	cu := os.Getenv("DB_USER") + ":" + os.Getenv("DB_PASSWORD") + "@tcp(" + os.Getenv("DB_HOST") + ")/" + os.Getenv("DB_TABLE")

	db, _ := sql.Open("mysql", cu)

	if db.Ping() != nil {
		fmt.Fprintf(w, "Unable to connection")
	} else {
		fmt.Fprintf(w, cu)
	}

	stmt, err := db.Prepare("CREATE DATABASE foo;")

	if err != nil {
		fmt.Fprintf(w, " Error preparing database foo")
	}

	stmt.Exec()

	tblsql := "CREATE TABLE foo.person( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(190) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;"
	stmt, _ = db.Prepare(tblsql)
	stmt.Exec()

	stmt, _ = db.Prepare("INSERT foo.person SET name=?")
	stmt.Exec("test")
	fmt.Fprintf(w, "Hello World")

	rows, _ := db.Query("SELECT * FROM foo.person")

	for rows.Next() {
		var uid int
		var username string

		err = rows.Scan(&uid, &username)
		fmt.Println(uid)
		fmt.Println(username)

		fmt.Fprintf(w, username)
	}

	stmt, _ = db.Prepare("delete from userinfo where uid=?")
	res, _ := stmt.Exec(0)
	affect, _ := res.RowsAffected()
	fmt.Println(affect)

	db.Close()

}

func testServe() {
	http.HandleFunc("/", helloWorld)
	http.ListenAndServe(":80", nil)
}
